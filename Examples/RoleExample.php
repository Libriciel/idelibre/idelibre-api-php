<?php


use IdelibreApiPhp\Client;
use IdelibreApiPhp\HttpClient\Builder;

require_once __DIR__ . '/../vendor/autoload.php';

$clientIdelibre = new Client('https://idelibre-4-3.recette.libriciel.net', new Builder());
$clientIdelibre->setAuthentication('myApitoken');
