<?php

/**
 * ? Initialisation du client Idelibre
 */

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\HttpClient\Builder;

require_once __DIR__ . '/../vendor/autoload.php';

$idelibreClient = new Client('https://idelibre-4-3.recette.libriciel.net', new Builder());

// ou //

$idelibreClient = new Client(
     'https://idelibre-4-3.recette.libriciel.net',
     new Builder(
         Psr18ClientDiscovery::find(),
         Psr17FactoryDiscovery::findRequestFactory(),
         Psr17FactoryDiscovery::findStreamFactory()
     )
 );


$idelibreClient->setAuthentication('myApitoken');


