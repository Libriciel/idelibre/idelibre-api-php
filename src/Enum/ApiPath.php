<?php
 namespace IdelibreApiPhp\Enum;

 enum ApiPath: string
 {
     public const CHECK_ENDPOINT = '/api/v2';
     public const BASE_API_V2_PATH = '/api/v2/structures/%s';
     public const USERS_PATH = '/api/users/sittings/%s';
     public const PROJECT_PATH = '/api/projects';
     public const OTHERDOC_PATH = 'api/otherdocs';
     public const CONVOCATIONS_PATH = '/api/convocations';

 }
