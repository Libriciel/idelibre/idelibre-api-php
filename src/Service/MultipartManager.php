<?php

namespace IdelibreApiPhp\Service;

use DateTimeZone;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use IdelibreApiPhp\Api\Exception\IdelibreException;

class MultipartManager
{
    public const HEADER_PDF = ['Content-Type' => 'multipart/form-data'];

    /**
     * @throws IdelibreException
     */
    public function prepareMultipartForSitting(array $sitting): MultipartStreamBuilder
    {
        $streamFactory = Psr17FactoryDiscovery::findStreamFactory();
        $builder = new MultipartStreamBuilder($streamFactory);

        foreach ($sitting as $key => $value) {
            match($key) {
                'convocationFile' =>
                    !empty($value) and file_exists($value['path'])
                        ? $builder->addResource($key, fopen($value['path'], 'r'), ['filename' => $value['name'], self::HEADER_PDF])
                        : throw new IdelibreException('La convocation est obligatoire'),

                'invitationFile' =>
                    !empty($value) and file_exists($value['path'])
                        ? $builder->addResource($key, fopen($value['path'], 'r'), ['filename' => $value['name'], self::HEADER_PDF])
                        : $builder->addResource($key, null),

                'place' => $value ? $builder->addResource($key, $value) : null,
                'date' => $builder->addResource($key, $this->formatDateTime($value['timestamp'])),
                'type' => $builder->addResource($key, $value['id']),
                'isRemoteAllowed' => $builder->addResource($key, $value === true ? '1' : '0'),
                'isMandatorAllowed' => $builder->addResource($key, $value === true ? '1' : '0'),

                default => 'no more property',
            };
        }

        return $builder;
    }


    public function prepareMultipartForDocuments(string $documentType , string $jsonDocumentType , array $fileArray, array $documentTypeArray, ?array $annexesArray): MultipartStreamBuilder
    {
        $streamFactory = Psr17FactoryDiscovery::findStreamFactory();
        $builder = new MultipartStreamBuilder($streamFactory);

        $builder->addResource($documentType, $jsonDocumentType);

        foreach ($fileArray as $file){
            $builder->addResource($this->getProjectLinkedFileKey($documentTypeArray, $file), fopen($file['path'], 'r'), ['filename' => $file['name'], self::HEADER_PDF]);
            $builder->addResource($this->getAnnexesLinkedFileKey($annexesArray, $file), fopen($file['path'], 'r'), ['filename' => $file['name'], self::HEADER_PDF]);
        }

        return $builder;
    }

    private function formatDateTime($value): string
    {
        return date('Y-m-d H:i:s', $value);
    }

    private function getProjectLinkedFileKey(array $projectArray, mixed $file): string
    {
        $linkedFileKey= '';
        foreach ($projectArray as $project){
            if ($file['id'] === $project['linkedFileKey']){
                $linkedFileKey = $project['linkedFileKey'];
            }
        }
        return $linkedFileKey;
    }

    private function getAnnexesLinkedFileKey(array $annexesArray, mixed $file): string
    {
        $linkedFileKey= '';
        foreach ($annexesArray as $annex){
            if ($file['id'] === $annex['linkedFileKey']){
                $linkedFileKey = $annex['linkedFileKey'];
            }
        }
        return $linkedFileKey;
    }



}
