<?php

namespace IdelibreApiPhp;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Exception;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Factory\Guzzle\RequestFactory;
use Http\Message\Authentication\Header;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use IdelibreApiPhp\Api\v2\Check;
use IdelibreApiPhp\HttpClient\Builder;
use IdelibreApiPhp\HttpClient\IdelibreExceptionPlugin;
use IdelibreApiPhp\Model\Api\UserApi;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;

class Client
{

    private Builder $httpClientBuilder;

    public function __construct(string $url = '', Builder $httpClientBuilder = null)
    {
        $this->httpClientBuilder = $httpClientBuilder ?: new Builder();
        if ($url) {
            $this->setUrl($url);
        }
    }

    public function setUrl(string $url): string
    {
        $this->getHttpClientBuilder()->addPlugin(new IdelibreExceptionPlugin());
        $this->getHttpClientBuilder()->addPlugin(new AddHostPlugin(Psr17FactoryDiscovery::findUriFactory()->createUri($url)));
        return $url;
    }

    public function setAuthentication(string $token): void
    {
        $this->getHttpClientBuilder()->removePlugin(AuthenticationPlugin::class);
        $this->getHttpClientBuilder()->addPlugin(new AuthenticationPlugin(new Header('x-auth-token', $token)));
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function isApiUser(): ?UserApi
    {
        $check = new Check($this);
        return $check->me();
    }

    protected function getHttpClientBuilder(): Builder
    {
        return $this->httpClientBuilder;
    }

    public function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->getHttpClientBuilder()->getHttpClient();
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->getHttpClientBuilder()->getStreamFactory();
    }

    /**
     * @param string $path
     * @return array
     * @throws ClientExceptionInterface
     */
    public function get(string $path): array
    {
        $response = $this->getHttpClient()->get($path);

        return $this->getContent($response);
    }

    /**
     * @throws Exception
     */
    public function getAsStream(string $path): StreamInterface
    {
        return $this->getHttpClient()->get($path)->getBody();
    }

    /**
     * @param string $path
     * @param object|array $data
     * @return array
     * @throws ClientExceptionInterface
     */
    public function post(string $path, object|array $data): array
    {
        $response = $this->getHttpClient()->post(
            $path,
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );

        return $this->getContent($response);
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     */
    public function postMultipart(string $path, MultipartStreamBuilder $builder): array
    {
        $requestFactory = new RequestFactory();
        $stream = $builder->build();

        $request = $requestFactory->createRequest('POST', $path)
            ->withHeader('Content-Type', 'multipart/form-data; boundary="' . $builder->getBoundary() . '"')
            ->withBody($stream)
        ;

        $response = $this->getHttpClient()->sendRequest($request);

        return $this->getContent($response);
    }



    /**
     * @param string $path
     * @param object|array $data
     * @return array
     * @throws ClientExceptionInterface
     */
    public function put(string $path, object|array $data): array
    {
        $response = $this->getHttpClient()->put(            $path,
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );

        return $this->getContent($response);
    }

    /**
     * @param string $path
     * @return array|null
     * @throws Exception
     */
    public function delete(string $path): array|null
    {
        $response = $this->getHttpClient()->delete($path);

        return $this->getContent($response);
    }

    private function getContent(ResponseInterface $response): array
    {
        $body = (string)$response->getBody();
        return json_decode($body, true);
    }
}
