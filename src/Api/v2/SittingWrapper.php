<?php

namespace IdelibreApiPhp\Api\v2;

use Http\Client\Exception;
use IdelibreApiPhp\Api\Exception\IdelibreException;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Annex;
use IdelibreApiPhp\Model\Api\OtherdocApi;
use IdelibreApiPhp\Model\Api\ProjectApi;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Convocation;
use IdelibreApiPhp\Model\File;
use IdelibreApiPhp\Model\Project;
use IdelibreApiPhp\Model\Sitting;
use IdelibreApiPhp\Model\Theme;
use IdelibreApiPhp\Model\User;
use IdelibreApiPhp\Service\MultipartManager;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SittingWrapper
{
    private readonly Client $client;
    private Serializer $serializer;
    private MultipartManager $multipartManager;
    private UserApi $userApi;


    /**
     * @throws ClientExceptionInterface
     */
    public function __construct(Client $client)
    {

        $this->client = $client;
        $this->userApi = $client->isApiUser();

        $this->multipartManager = new MultipartManager();
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor(), null, null, [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
        ]);
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer(
            [$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(), $dateTimeZoneNormalizer],
            [new JsonEncoder()],
        );
    }


    /**
     * @throws ClientExceptionInterface
     */
    public function getAll(): array
    {
        $response =  $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/sittings', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $sittings = [];
        foreach ($response as $sitting) {
            $sittings[] = $this->serializer->denormalize($sitting, Sitting::class, 'json');
        }

        return $sittings;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function getOne(string $sittingId): ?Sitting
    {
        $response =  $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId, $this->userApi->getStructure()->getId()));

        return $this->serializer->deserialize($response->getBody()->getContents(), Sitting::class, 'json');
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function getAllConvocations($sittingId): array
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId . '/convocations', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $convocations = [];
        foreach ($response as $convocation) {
            $convocations[] = $this->serializer->denormalize($convocation, Convocation::class, 'json');
        }

        return $convocations;
    }

    /**
     * @throws Exception
     */
    public function sendAll(string $sittingId)
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId . '/convocations/sendAll', $this->userApi->getStructure()->getId()));

        return $this->serializer->decode($response->getBody()->getContents(), 'json');
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function getAllProjects(string $sittingId): array
    {
        $response =  $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId . '/projects', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $projects = [];
        foreach ($response as $project) {

            $reporter = $project['reporter'] ? $this->serializer->denormalize($project['reporter'], User::class, 'json') : null;

            $annexes = [];
            if ($project['annexes']){
                foreach ($project['annexes'] as $annex){
                    $annexes[] = $this->serializer->denormalize($annex, Annex::class, 'json');
                }
            }

            $project = $this->serializer->denormalize($project, Project::class, 'json');
            $project->setReporter($reporter);
            $project->addAnnexes($annexes);

            $projects[] = $project;
        }

        return $projects;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     * @throws IdelibreException
     */
    public function create(Sitting $sitting): ?Sitting
    {
       $type = $this->serializer->normalize($sitting->getType(), null, [
            'fields' => ['id, name']
        ]);

        $convocationFile = $this->serializer->normalize($sitting->getConvocationFile());
        $sitting->getInvitationFile() ? $invitationFile = $this->serializer->normalize($sitting->getInvitationFile()) : $invitationFile = null;

        $sitting = $this->serializer->normalize($sitting, null, [
            'fields' => ['date, place, convocationFile, invitationFile, reminder, isRemoteAllowed, isMandatorAllowed']
        ]);

        $sitting['type'] = $type;
        $sitting['convocationFile'] = $convocationFile;
        $sitting['invitationFile'] = $invitationFile;

        $builder = $this->multipartManager->prepareMultipartForSitting($sitting);

        $response = $this->client->postMultipart(
            sprintf(ApiPath::BASE_API_V2_PATH . '/sittings', $this->userApi->getStructure()->getId()),
            $builder
        );

        return $this->serializer->denormalize($response, Sitting::class, 'json');

    }

    /**
     * @throws ExceptionInterface
     * @throws ClientExceptionInterface
     * @throws IdelibreException
     */
    public function edit(Sitting $sitting): ?Sitting
    {
        $sittingId = $sitting->getId();
        $type = $this->serializer->normalize($sitting->getType(), null, [
            'fields' => ['id, name, isSmsGuests, isSmsEmployees, issms, isComelus']
        ] );

        $convocationFile = $this->serializer->normalize($sitting->getConvocationFile());
        $sitting->getInvitationFile() ? $invitationFile = $this->serializer->normalize($sitting->getInvitationFile()) : $invitationFile = null;

        $sitting = $this->serializer->normalize($sitting, null, [
            'fields' => ['date, place, convocationFile, invitationFile, reminder, isRemoteAllowed, isMandatorAllowed']
        ]);

        $sitting['type'] = $type;
        $sitting['convocationFile'] = $convocationFile;
        $sitting['invitationFile'] = $invitationFile;

        $builder = $this->multipartManager->prepareMultipartForSitting($sitting);

        $response = $this->client->postMultipart(
            sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId , $this->userApi->getStructure()->getId()),
            $builder
        );

        return $this->serializer->denormalize($response, Sitting::class, 'json');
    }

    /**
     * @param string $sittingId
     * @param array $projects
     * @param array $files
     * @return array
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function addProjects(string $sittingId, array $projects, array $files): array
    {
        $fieldProjects = ['id, name, rank, file, reporterId, themeId, annexes, linkedFileKey, filename, path'];
        $fieldsFiles =  ['id, name, path, size'];

        $projectsArray = $this->documentsNormalizer($projects, $fieldProjects);
        $annexesArray = $this->extractAnnexes($projectsArray);
        $fileArray = $this->documentsNormalizer($files, $fieldsFiles);


        $projects = $this->serializer->encode($projectsArray, 'json');

        $builder = $this->multipartManager->prepareMultipartForDocuments("projects", $projects, $fileArray, $projectsArray, $annexesArray);

        $response = $this->client->postMultipart(
            sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId . '/projects' , $this->userApi->getStructure()->getId()),
            $builder
        );

        $projectsArray = [];
        foreach ($response as $project) {
            $projectsArray[] = $this->serializer->denormalize($project, ProjectApi::class);
        }

        return $projectsArray;
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    public function addOtherdocs(string $sittingId, array $otherdocs, array $files): array
    {
        $fieldsOtherdocs = ['id, name, rank, file, filename, size, linkedFileKey'];
        $fieldsFiles =  ['id, name, path, size'];

        $otherdocArray = $this->documentsNormalizer($otherdocs, $fieldsOtherdocs);
        $fileArray = $this->documentsNormalizer($files, $fieldsFiles);
        $otherdocs = $this->serializer->encode($otherdocArray, 'json');

        $builder = $this->multipartManager->prepareMultipartForDocuments("otherdocs" , $otherdocs, $fileArray, $otherdocArray, []);

        $response = $this->client->postMultipart(
            sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId . '/otherdocs' , $this->userApi->getStructure()->getId()),
            $builder
        );

        $otherdocArray = [];
        foreach ($response as $otherdoc){
           $otherdocArray[] = $this->serializer->denormalize($otherdoc, OtherdocApi::class);
        }
        return $otherdocArray;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function deleteProject(string $sittingId, string $projectId): int
    {
        $response = $this->client->getHttpClient()->delete(
            sprintf(ApiPath::BASE_API_V2_PATH . '/sittings/' . $sittingId . '/projects/' . $projectId, $this->userApi->getStructure()->getId())
        );

        return $response->getStatusCode();
    }

    /**
     * @throws ExceptionInterface
     */
    private function documentsNormalizer(array $documents, array $fields): array
    {
        $documentsArray = [];
        foreach ($documents as $document) {
            $documentsArray[] = $this->serializer->normalize($document, null, [
                'fields' => $fields
            ]);
        }
        return $documentsArray;
    }

    private function extractAnnexes(array $projectArray): array
    {
        $annexesArray = [];
        foreach ($projectArray as $project){

            if ($project['annexes'] = []) {
                continue;
            }

            foreach ($project['annexes'] as $annex){
                $annexesArray[] = $annex;
            }

        }
        return $annexesArray;
    }

}
