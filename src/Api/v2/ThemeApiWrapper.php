<?php

namespace IdelibreApiPhp\Api\v2;

use Http\Client\Exception;
use IdelibreApiPhp\Api\SerializerHelper\DenormalizerHelper;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Theme;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ThemeApiWrapper
{
    private Serializer $serializer;
    private Client $client;
    private UserApi $userApi;

    /**
     * @throws ClientExceptionInterface
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(),$dateTimeZoneNormalizer], [new JsonEncoder()]);
        $this->userApi = $client->isApiUser();
    }

    /**
     * @throws Exception
     */
    public function getAll(): array
    {
        $response =  $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/themes', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $themes = [];
        foreach ($response as $theme) {
            $themes[] = $this->serializer->denormalize($theme,Theme::class, 'json');
        }

        return $themes;
    }

    /**
     * @throws Exception
     */
    public function getOne(string $themeId): ?Theme
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/themes/' . $themeId, $this->userApi->getStructure()->getId()));

        return $this->serializer->deserialize($response->getBody()->getContents(), Theme::class, 'json');
    }

    /**
     * @throws Exception
     */
    public function create(Theme $theme): Theme
    {
        $response = $this->client->getHttpClient()->post(
            sprintf(ApiPath::BASE_API_V2_PATH . '/themes', $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($theme, 'json')
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), Theme::class, 'json');
    }

    /**
     * @throws Exception
     */
    public function edit(Theme $theme): Theme
    {
        $response = $this->client->getHttpClient()->put(
            sprintf(ApiPath::BASE_API_V2_PATH . '/themes/' . $theme->getId(), $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($theme, 'json')
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), Theme::class, 'json');
    }

    /**
     * @throws Exception
     */
    public function delete(string $themeId): int
    {
        $response =  $this->client->getHttpClient()->delete(
            sprintf(ApiPath::BASE_API_V2_PATH . '/themes/' . $themeId, $this->userApi->getStructure()->getId())
        );

        return $response->getStatusCode();
    }

}
