<?php

namespace IdelibreApiPhp\Api\v2;

use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Role;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RoleApiWrapper
{

    private Client $client;
    private Serializer $serializer;
    private UserApi $userApi;

    /**
     * @throws ClientExceptionInterface
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(),$dateTimeZoneNormalizer], [new JsonEncoder()]);
        $this->userApi = $client->isApiUser();
    }


    /**
     * @return Role[]
     * @throws ClientExceptionInterface
     */
    public function getAll(): array
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/roles', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $roles = [];
        foreach ($response as $role) {
            $roles[] = $this->serializer->denormalize($role, Role::class);
        }

        return $roles;
    }


}
