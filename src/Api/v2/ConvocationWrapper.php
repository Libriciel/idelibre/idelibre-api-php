<?php

namespace IdelibreApiPhp\Api\v2;

use Http\Client\Exception;
use IdelibreApiPhp\Api\SerializerHelper\DenormalizerHelper;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Convocation;
use IdelibreApiPhp\Model\Theme;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ConvocationWrapper
{
    private Client $client;
    private Serializer $serializer;
    private DenormalizerHelper $denormalizeHelper;
    private UserApi $userApi;

    /**
     * @throws ClientExceptionInterface
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(),$dateTimeZoneNormalizer], [new JsonEncoder()]);
        $this->userApi = $client->isApiUser();
        $this->denormalizeHelper = new DenormalizerHelper();
    }




    /**
     * @throws Exception
     */
    public function sendOne(string $convocationId): ?Convocation
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/convocations/' . $convocationId . '/sendOne', $this->userApi->getStructure()->getId()));

        return $this->serializer->deserialize($response->getBody()->getContents(), Convocation::class, 'json');
    }

}
