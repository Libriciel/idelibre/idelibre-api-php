<?php

namespace IdelibreApiPhp\Api\v2;

use IdelibreApiPhp\Api\SerializerHelper\DenormalizerHelper;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Party;
use IdelibreApiPhp\Model\Role;
use IdelibreApiPhp\Model\User;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class PartyApiWrapper
{

    private Client $client;
    private Serializer $serializer;
    private DenormalizerHelper $denormalizeHelper;
    private UserApi $userApi;

    /**
     * @throws ClientExceptionInterface
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->userApi = $client->isApiUser();
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(),$dateTimeZoneNormalizer], [new JsonEncoder()]);
        $this->denormalizeHelper = new DenormalizerHelper();
    }



    /**
     * @throws ClientExceptionInterface
     */
    public function getAll(): array
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/parties', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $parties = [];
        foreach ($response as $party) {
            $parties[] = $this->serializer->denormalize($party, Party::class);
        }

        return $parties;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function getOne(string $partyId): ?Party
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/parties/' . $partyId, $this->userApi->getStructure()->getId()));

        return $this->serializer->deserialize($response->getBody()->getContents(), Party::class, 'json');
    }


    /**
     * @throws ExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function create(Party $party): ?Party
    {
        $response =  $this->client->getHttpClient()->post(
            sprintf(ApiPath::BASE_API_V2_PATH . '/parties', $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($party, 'json')
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), Party::class, 'json');
    }

    /**
     * @throws ExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function edit(Party $party): ?Party
    {
        $response =  $this->client->getHttpClient()->put(
            sprintf(ApiPath::BASE_API_V2_PATH . '/parties/' . $party->getId() , $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($party, 'json')
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), Party::class, 'json');
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function delete(string $partyId): int
    {
       $response =  $this->client->getHttpClient()->delete(
           sprintf(ApiPath::BASE_API_V2_PATH . '/parties/' . $partyId, $this->userApi->getStructure()->getId())
       );

       return $response->getStatusCode();
    }
}

