<?php

namespace IdelibreApiPhp\Api\v2;

use Http\Client\Exception;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Api\RoleApi;
use IdelibreApiPhp\Model\Api\UserApi;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Check
{
    private Client $client;
    private Serializer $serializer;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $getSetNormalizer = new GetSetMethodNormalizer();
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer()], [new JsonEncoder()]);
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     */
    public function ping(): string
    {
        return $this->client->getHttpClient()->get(ApiPath::CHECK_ENDPOINT . '/ping')->getBody()->getContents();
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function me(): ?UserApi
    {
        $response =  $this->client->getHttpClient()->get(ApiPath::CHECK_ENDPOINT . '/me');
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        return $this->serializer->denormalize($response, UserApi::class);
    }

}
