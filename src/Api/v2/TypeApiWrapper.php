<?php

namespace IdelibreApiPhp\Api\v2;

use Exception;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Reminder;
use IdelibreApiPhp\Model\Type;
use IdelibreApiPhp\Model\User;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class TypeApiWrapper
{
    private Client $client;
    private Serializer $serializer;
    private UserApi $userApi;

    /**
     * @throws ClientExceptionInterface
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function (object $object, ?string $format, array $context): string {
                return $object->getId();
            },
            AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT => 1,
        ];
        $objectNormalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(),$dateTimeZoneNormalizer], [new JsonEncoder()]);
        $this->userApi = $client->isApiUser();
    }


    /**
     * @throws ClientExceptionInterface
     */
    public function getAll(): array
    {
        $response =  $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/types', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $types = [];
        foreach ($response as $type) {
            $types[] = $this->serializer->denormalize($type, Type::class, 'json');
        }

        return $types;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function getOne(string $typeId): ?Type
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/types/' . $typeId, $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $response['reminder'] = $this->serializer->denormalize($response['reminder'], Reminder::class);
        $type = $this->serializer->denormalize($response, Type::class, 'json');
        $type->setAssociatedUsers(array_map(fn($user) => $this->serializer->denormalize($user, User::class), $response['associatedUsers']));
        $type->setReminder($response['reminder']);

        return $type;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function create(Type $type): Type
    {
        $response = $this->client->getHttpClient()->post(
            sprintf(ApiPath::BASE_API_V2_PATH . '/types', $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($type, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ])
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), Type::class, 'json');
    }

    /**
     * @throws ClientExceptionInterface|ExceptionInterface
     */
    public function edit(Type $type): Type
    {
        $response = $this->client->getHttpClient()->put(
            sprintf(ApiPath::BASE_API_V2_PATH . '/types/' . $type->getId(), $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($type, 'json',[
                [
                    'circular_reference_handler' => function ($object) {
                        return $object->getId();
                    }
                ]
            ])
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), Type::class, 'json');
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function delete(string $typeId): int
    {
        $response = $this->client->getHttpClient()->delete(
            sprintf(ApiPath::BASE_API_V2_PATH . '/types/' . $typeId, $this->userApi->getStructure()->getId())
        );

        return $response->getStatusCode();
    }

}
