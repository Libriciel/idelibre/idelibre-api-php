<?php

namespace IdelibreApiPhp\Api\v2;

use Exception;
use IdelibreApiPhp\Api\SerializerHelper\DenormalizerHelper;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Party;
use IdelibreApiPhp\Model\Role;
use IdelibreApiPhp\Model\User;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class UserApiWrapper
{

    private readonly Client $client;
    private Serializer $serializer;
    private DenormalizerHelper $denormalizerHelper;
    private UserApi $userApi;

    /**
     * @throws ClientExceptionInterface
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(),$dateTimeZoneNormalizer], [new JsonEncoder()]);
        $this->userApi = $client->isApiUser();
        $this->denormalizerHelper = new DenormalizerHelper();
    }



    /**
     * @return UserApi[]
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function getAll(): array
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/users', $this->userApi->getStructure()->getId()));
        $response = $this->serializer->decode($response->getBody()->getContents(), 'json');

        $users = [];
        foreach ($response as $user) {
            $user['deputy'] ? $this->serializer->denormalize($user['deputy'], User::class) : $user['deputy'] = null;
            $users[] = $this->serializer->denormalize($user, User::class);//
        }

        return $users;
    }


    /**
     * @param string $userId
     * @return User|null
     * @throws ClientExceptionInterface
     */
    public function getOne(string $userId) : ?User
    {
        $response = $this->client->getHttpClient()->get(sprintf(ApiPath::BASE_API_V2_PATH . '/users/' . $userId, $this->userApi->getStructure()->getId()));

        return $this->serializer->deserialize($response->getBody()->getContents(), User::class, 'json');
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    public function create(User $user) : ?User
    {
        $response = $this->client->getHttpClient()->post(
            sprintf(ApiPath::BASE_API_V2_PATH . '/users', $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($user, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ])
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), User::class, 'json');
    }




    /**
     * @throws ExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function edit(User $user) : ?User
    {
        $response = $this->client->getHttpClient()->put(
            sprintf(ApiPath::BASE_API_V2_PATH . '/users/' . $user->getId() , $this->userApi->getStructure()->getId()),
            [],
            $this->serializer->serialize($user, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ])
        );

        return $this->serializer->deserialize($response->getBody()->getContents(), User::class, 'json');
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function delete(string $userApiId): int
    {
        $response = $this->client->getHttpClient()->delete(sprintf(ApiPath::BASE_API_V2_PATH . '/users/' . $userApiId, $this->userApi->getStructure()->getId()));

        return $response->getStatusCode();
    }

}
