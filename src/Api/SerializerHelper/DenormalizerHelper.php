<?php

namespace IdelibreApiPhp\Api\SerializerHelper;

use DateTime;
use IdelibreApiPhp\Model\File;
use IdelibreApiPhp\Model\Group;
use IdelibreApiPhp\Model\Party;
use IdelibreApiPhp\Model\Reminder;
use IdelibreApiPhp\Model\Role;
use IdelibreApiPhp\Model\Sitting;
use IdelibreApiPhp\Model\Structure;
use IdelibreApiPhp\Model\Timezone;
use IdelibreApiPhp\Model\Type;
use IdelibreApiPhp\Model\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DenormalizerHelper
{
    public function denormalizeUser($key, $value, $serializer)
    {
        if ($key === 'user' or $key === 'mandator' or $key === 'deputy') {
            $value['role'] = $serializer->denormalize($value['role'], Role::class);
            $value['party'] = $serializer->denormalize($value['party'], Party::class);
        }
        return $serializer->denormalize($value, User::class);
    }

    /**
     * @throws \DateMalformedStringException
     */
    public function denormalizeSitting($key, $value, $serializer)
    {
        if ($key === 'sitting') {
            $value['type'] = $serializer->denormalize($value['type'], Type::class);
            $value['date'] = new DateTime($value['date']);
            $value['convocationFile'] = $serializer->denormalize($value['convocationFile'], File::class);
            isset($value['invitationFile']) ? $value['invitationFile'] = $serializer->denormalize($value['invitationFile'], File::class) : $value['invitationFile'] = null;
            $value['reminder'] = $serializer->denormalize($value['reminder'], Reminder::class);

        }
        return $serializer->denormalize($value, Sitting::class);
    }

    public function denormalizeStructure($key, $value, $serializer)
    {
        if ($key === 'structure') {
            isset($value['timezone']) ?? $value['timezone'] = $serializer->denormalize($value['timezone'], Timezone::class);
            isset($value['group']) ?? $value['group'] = $serializer->denormalize($value['group'], Group::class);
        }
        return $serializer->denormalize($value, Structure::class);
    }
}
