<?php

namespace IdelibreApiPhp\Api\Exception;
use Exception;

class UnauthorizedException extends Exception
{
}
