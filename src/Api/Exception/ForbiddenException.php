<?php

namespace IdelibreApiPhp\Api\Exception;

use Exception;

class ForbiddenException extends Exception
{
}
