<?php

namespace IdelibreApiPhp\Api\Exception;
use Exception;

class NotFoundException extends Exception
{
}
