<?php

namespace IdelibreApiPhp\Model;

class Reminder
{
    public const VALUES = [
        '30 minutes' => 30,
        '1 heure' => 60,
        '90 minutes' => 90,
        '2 heures' => 120,
        '3 heures' => 180,
        '4 heures' => 240,
        '5 heures' => 300,
    ];

    private $id;
    private int $duration = 60;
    private bool $isActive = false;
    private ?Sitting $sitting;
    private ?Type $type;

    public function __construct()
    {
        $this->sitting = null;
        $this->type = null;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        if (!$duration) {
            return $this;
        }

        $this->duration = $duration;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getSitting(): ?Sitting
    {
        return $this->sitting;
    }

    public function setSitting(Sitting $sitting): self
    {
        $this->sitting = $sitting;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(Type $type): self
    {
        $this->type = $type;

        return $this;
    }
}
