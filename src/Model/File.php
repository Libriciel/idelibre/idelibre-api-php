<?php

namespace IdelibreApiPhp\Model;

use DateTimeImmutable;

class File
{
    private $id;
    private ?string $path;
    private ?float $size;
    private ?string $name;
    private DateTimeImmutable $createdAt;
    private ?Project $project;
    private ?Annex $annex;
    private $convocationSitting;
    private $invitationSitting;
    private $otherdoc;



    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }


    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getSize(): ?float
    {
        return $this->size;
    }

    public function setSize(?float $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getStructure(): Structure
    {
        if ($this->project) {
            return $this->project->getSitting()->getStructure();
        }
        if ($this->annex) {
            return $this->annex->getProject()->getSitting()->getStructure();
        }

        if ($this->convocationSitting) {
            return $this->convocationSitting->getStructure();
        }

        if ($this->otherdoc) {
            return $this->otherdoc->getSitting()->getStructure();
        }

        return $this->invitationSitting->getStructure();
    }
}
