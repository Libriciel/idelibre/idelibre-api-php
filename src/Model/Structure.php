<?php

namespace IdelibreApiPhp\Model;

class Structure
{
    private ?string $id;
    private ?string $name;
    private ?string $replyTo;
    private ?string $siren;
    private array $users;
    private ?Group $group;
    private ?Timezone $timezone;
    private ?string $suffix;
    private bool $isActive = true;
    private ?bool $canEditReplyTo = true;

    public function __construct()
    {
        $this->users = [];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

   public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReplyTo(): ?string
    {
        return $this->replyTo;
    }

    public function setReplyTo(?string $replyTo): self
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!in_array($user, $this->users, true)) {
            $this->users[] = $user;
            $user->setStructure($this);
        }

        return $this;
    }

    public function getGroup(): ?Group
    {
        return $this->group;
    }

    public function setGroup(?Group $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getTimezone(): ?Timezone
    {
        return $this->timezone;
    }

    public function setTimezone(?Timezone $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    public function setSuffix(string $suffix): self
    {
        $this->suffix = $suffix;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren($siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function isCanEditReplyTo(): ?bool
    {
        return $this->canEditReplyTo;
    }

    public function setCanEditReplyTo(bool $canEditReplyTo): self
    {
        $this->canEditReplyTo = $canEditReplyTo;

        return $this;
    }
}
