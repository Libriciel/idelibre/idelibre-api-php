<?php

namespace IdelibreApiPhp\Model;



class Group
{
    private ?string $id;
    private ?string $name;

    private array $structures;
    private bool $isStructureCreator = false;

    public function __construct()
    {
        $this->structures = [];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getStructures(): array
    {
        return $this->structures;
    }

    public function getIsStructureCreator(): ?bool
    {
        return $this->isStructureCreator;
    }

}
