<?php

namespace IdelibreApiPhp\Model\Api;


class OtherdocApi
{
    private ?string $id = null;
    private ?string $name;
    private ?string $linkedFileKey = null;
    private ?string $fileName = null;
    private ?int $size = null;
    private ?int $rank = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): OtherdocApi
    {
        $this->name = $name;

        return $this;
    }

    public function getLinkedFileKey(): ?string
    {
        return $this->linkedFileKey;
    }

    public function setLinkedFileKey(?string $linkedFileKey): OtherdocApi
    {
        $this->linkedFileKey = $linkedFileKey;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(?int $size): OtherdocApi
    {
        $this->size = $size;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): OtherdocApi
    {
        $this->rank = $rank;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): OtherdocApi
    {
        $this->id = $id;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(?string $fileName): OtherdocApi
    {
        $this->fileName = $fileName;

        return $this;
    }
}
