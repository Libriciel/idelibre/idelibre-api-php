<?php

namespace IdelibreApiPhp\Model\Api;

use Symfony\Component\Serializer\Attribute\Groups;

class RoleApi
{
    private string $id;
    private string $name;
    private array $composites = [];
    private string $prettyName;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getComposites(): ?array
    {
        return $this->composites;
    }

    public function setComposites(array $composites): self
    {
        $this->composites = $composites;

        return $this;
    }

    public function getPrettyName(): ?string
    {
        return $this->prettyName;
    }

    public function setPrettyName(string $prettyName): self
    {
        $this->prettyName = $prettyName;

        return $this;
    }
}
