<?php

namespace IdelibreApiPhp\Model\Api;

use IdelibreApiPhp\Model\Structure;

class UserApi
{
    private string $id;
    private string $name;
    private string $token;
    private Structure $structure;
    private RoleApi $apiRole;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->getApiRole() ? $this->getApiRole()->getComposites() : [];

        return array_unique($roles);
    }

    public function getPassword(): ?string
    {
        return null;
    }


    public function getUsername(): string
    {
        return $this->name;
    }

    public function getUserIdentifier(): string
    {
        return $this->getName();
    }

    public function getApiRole(): ?RoleApi
    {
        return $this->apiRole;
    }

    public function setApiRole(?RoleApi $apiRole): self
    {
        $this->apiRole = $apiRole;

        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

}
