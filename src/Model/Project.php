<?php

namespace IdelibreApiPhp\Model;

class Project
{
    private $id;
    private ?string $name;
    private ?int $rank;
    private ?File $file;
    private ?Theme $theme;
    private ?User $reporter;
    private ?Sitting $sitting;
    private array $annexes;

    public function __construct()
    {
        $this->annexes = [];
        $this->reporter = null;
        $this->theme = null;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getTheme(): ?Theme
    {
        return $this->theme;
    }

    public function setTheme(?Theme $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getReporter(): ?User
    {
        return $this->reporter;
    }

    public function setReporter(?User $reporter): self
    {
        $this->reporter = $reporter;

        return $this;
    }


    /**
     * @return Annex[]
     */
    public function getAnnexes(): array
    {
        return $this->annexes;
    }

    public function addAnnex(Annex $annex): self
    {
        if (!in_array($annex, $this->annexes, true)) {
            $this->annexes[] = $annex;
            $annex->setProject($this);
        }

        return $this;
    }

    /**
     * @param Annex[] $annexes
     */
    public function addAnnexes(array $annexes): self
    {
        foreach ($annexes as $annex) {
            $this->addAnnex($annex);
        }

        return $this;
    }

}
