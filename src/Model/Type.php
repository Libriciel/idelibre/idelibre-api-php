<?php

namespace IdelibreApiPhp\Model;

use DateTimeImmutable;

class Type
{
    private $id;
    private ?string $name;
    private array $associatedUsers;
    private ?Structure $structure;
    private array $authorizedSecretaries;
    private bool $isSms = false;
    private bool $isComelus = false;
    private Reminder|null $reminder = null;
    private ?bool $isSmsGuests = false;
    private ?bool $isSmsEmployees = false;
    private ?DateTimeImmutable $createdAt = null;
    private ?DateTimeImmutable $updatedAt = null;


    public function __construct()
    {
        $this->associatedUsers = [];
        $this->authorizedSecretaries = [];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getAssociatedUsers(): array
    {
        return $this->associatedUsers;
    }

    public function setAssociatedUsers(array $users): self
    {
        $this->associatedUsers = $users;

        return $this;
    }

    public function addAssociatedUser(User $associatedUser): self
    {
        if (!in_array($associatedUser, $this->associatedUsers, true)) {
            $this->associatedUsers[] = $associatedUser;
        }

        return $this;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getAuthorizedSecretaries(): array
    {
        return $this->authorizedSecretaries;
    }

    public function addAuthorizedSecretary(User $authorizedSecretary): self
    {
        if (!in_array($authorizedSecretary, $this->authorizedSecretaries, true)) {
            $this->authorizedSecretaries[] = $authorizedSecretary;
        }

        return $this;
    }

    public function getIsSms(): bool
    {
        return $this->isSms ?? false;
    }

    public function setIsSms(bool $isSms): self
    {
        $this->isSms = $isSms;

        return $this;
    }

    public function getIsComelus(): bool
    {
        return $this->isComelus ?? false;
    }

    public function setIsComelus(?bool $isComelus): self
    {
        $this->isComelus = $isComelus;

        return $this;
    }

    public function getReminder(): Reminder|null
    {
        return $this->reminder;
    }

    public function setReminder(Reminder|null $reminder): self
    {
        // set the owning side of the relation if necessary
        if ($reminder->getType() !== $this) {
            $reminder->setType($this);
        }

        $this->reminder = $reminder;

        return $this;
    }

    public function setIsSmsGuests(?bool $isSmsGuests): self
    {
        $this->isSmsGuests = $isSmsGuests;

        return $this;
    }

    public function getIsSmsGuests(): bool
    {
        return $this->isSmsGuests ?? false;
    }

    public function setIsSmsEmployees(?bool $isSmsEmployees): self
    {
        $this->isSmsEmployees = $isSmsEmployees;

        return $this;
    }

    public function getIsSmsEmployees(): bool
    {
        return $this->isSmsEmployees ?? false;
    }


    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAtValue(): void
    {
        $this->updatedAt = new DateTimeImmutable();
    }

}
