<?php

namespace IdelibreApiPhp\Model;

use DateTimeImmutable;


class User
{
    public string $id;
    private ?string $username;
    private ?string $email;
    private ?string $firstName;
    private ?string $lastName;
    private ?Structure $structure;

    public string|Role $role;
    private ?Party $party;
    private ?string $title;
    private ?int $gender;
    private bool $isActive = true;
    private ?string $phone;
    private ?self $deputy = null;
    private ?self $titular = null;
    private ?DateTimeImmutable $createdAt = null;
    private ?DateTimeImmutable $updatedAt = null;



    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setIdForTest(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }


    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getRole(): string|Role
    {
        return $this->role;
    }

    public function setRole(string|Role $role): self
    {
        $this->role = $role;

        return $this;
    }


    public function getParty(): ?Party
    {
        return $this->party;
    }

    public function setParty(?Party $party): self
    {
        $this->party = $party;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(?int $gender): self
    {
        $this->gender = $gender;

        return $this;
    }


    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDeputy(): ?self
    {
        return $this->deputy;
    }

    public function setDeputy(?self $deputy): static
    {
        $this->deputy = $deputy;

        return $this;
    }

    public function getTitular(): ?self
    {
        return $this->titular;
    }

    public function setTitular(?self $titular): static
    {
        // unset the owning side of the relation if necessary
        if ($titular === null && $this->titular !== null) {
            $this->titular->setDeputy(null);
        }

        // set the owning side of the relation if necessary
        if ($titular !== null && $titular->getDeputy() !== $this) {
            $titular->setDeputy($this);
        }

        $this->titular = $titular;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAtValue(): void
    {
        $this->updatedAt = new DateTimeImmutable();
    }

}
