<?php

namespace IdelibreApiPhp\Model;


use DateTimeImmutable;

class LsvoteSitting
{
    private ?string $id = null;
    private ?string $lsvoteSittingId = null;
    private array $results = [];
    private ?DateTimeImmutable $createdAt = null;
    private ?Sitting $sitting = null;


    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable('now');
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLsvoteSittingId(): ?string
    {
        return $this->lsvoteSittingId;
    }

    public function setLsvoteSittingId(string $lsvoteSittingId): self
    {
        $this->lsvoteSittingId = $lsvoteSittingId;

        return $this;
    }

    public function getResults(): array
    {
        return $this->results;
    }

    public function setResults(array $results): self
    {
        $this->results = $results;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }


    public function getSitting(): ?Sitting
    {
        return $this->sitting;
    }

    public function setSitting(?Sitting $sitting): self
    {
        $this->sitting = $sitting;

        return $this;
    }
}
