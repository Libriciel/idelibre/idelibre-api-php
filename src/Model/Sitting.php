<?php

namespace IdelibreApiPhp\Model;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;

class Sitting
{

    private string $id;
    private ?string $name;
    private ?DateTime $date;
    private ?string $place;
    private DateTimeImmutable $createdAt;
    private array $convocations;
    private Type $type;
    private ?Structure $structure;
    private ?File $convocationFile;

    /** * @var Project[] */
    private  array $projects;
    private ?File $invitationFile;

    /** * @var Otherdoc[] */
    private array $otherdocs;
    private bool $isRemoteAllowed = false;
    private ?DateTimeImmutable $updatedAt = null;
    private ?bool $isMandatorAllowed = true;

    private ?Reminder $reminder;

    public function __construct()
    {
        $this->id = '';
        $this->invitationFile = null;
        $this->createdAt = new DateTimeImmutable();
        $this->convocations = [];
        $this->projects = [];
        $this->otherdocs = [];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function setDate(?DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }


    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return Convocation[]
     */
    public function getConvocations(): array
    {
        return $this->convocations;
    }

    public function addConvocation(Convocation $convocation): self
    {
        if (!in_array($convocation, $this->convocations, true)) {
            $this->convocations[] = $convocation;
            $convocation->setSitting($this->convocationFile);
        }

        return $this;
    }



    public function getType(): Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getConvocationFile(): ?File
    {
        return $this->convocationFile;
    }

    public function setConvocationFile(?File $convocationFile): self
    {
        $this->convocationFile = $convocationFile;

        return $this;
    }

    /**
     * @return Project[]
     */
    public function getProjects(): array
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!in_array($project, $this->projects, true)) {
            $this->projects[] = $project;
            $project->setSitting($this);
        }

        return $this;
    }

    public function getInvitationFile(): ?File
    {
        return $this->invitationFile;
    }

    public function setInvitationFile(?File $invitationFile): self
    {
        $this->invitationFile = $invitationFile;

        return $this;
    }


    /**
     * @return Otherdoc[]
     */
    public function getOtherdocs(): array
    {
        return $this->otherdocs;
    }

    public function addOtherdoc(Otherdoc $otherdoc): self
    {
        if (!in_array($otherdoc, $this->otherdocs, true)) {
            $this->otherdocs[] = $otherdoc;
            $otherdoc->setSitting($this);
        }

        return $this;
    }


    public function getIsRemoteAllowed(): bool
    {
        return $this->isRemoteAllowed;
    }

    public function setIsRemoteAllowed(bool $isRemoteAllowed): self
    {
        $this->isRemoteAllowed = $isRemoteAllowed;

        return $this;
    }


    public function setCreatedAtValue(): void
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->setUpdatedAtValue();
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAtValue(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function isMandatorAllowed(): ?bool
    {
        return $this->isMandatorAllowed;
    }

    public function setIsMandatorAllowed(bool $isMandatorAllowed): self
    {
        $this->isMandatorAllowed = $isMandatorAllowed;

        return $this;
    }

    public function getReminder(): ?Reminder
    {
        return $this->reminder;
    }

    public function setReminder(Reminder $reminder): self
    {
        // set the owning side of the relation if necessary
        if ($reminder->getSitting() !== $this) {
            $reminder->setSitting($this);
        }

        $this->reminder = $reminder;

        return $this;
    }

}
