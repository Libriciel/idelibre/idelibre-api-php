<?php

namespace IdelibreApiPhp\Model;


use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;

class Convocation
{
    public const CATEGORY_CONVOCATION = 'convocation';
    public const CATEGORY_INVITATION = 'invitation';
    public const CATEGORY_DELEGATION = 'delegation';
    public const PRESENT = 'present';
    public const REMOTE = 'remote';
    public const ABSENT = 'absent';
    public const ABSENT_GIVE_POA = 'poa';
    public const ABSENT_SEND_DEPUTY = 'deputy';
    public const UNDEFINED = '';

    private $id;
    private bool $isRead = false;
    private DateTimeImmutable $createdAt;
    private bool $isActive = false;
    private bool $isEmailed = false;
    private ?Sitting $sitting;
    private ?User $user;
    private ?string $category;
    private ?string $attendance;
    private ?User $deputy = null;
    private ?User $mandator = null;


    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->attendance = '';
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getIsRead(): ?bool
    {
        return $this->isRead;
    }

    public function setIsRead(bool $isRead): self
    {
        $this->isRead = $isRead;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsEmailed(): ?bool
    {
        return $this->isEmailed;
    }

    public function setIsEmailed(?bool $isEmailed): self
    {
        $this->isEmailed = $isEmailed;

        return $this;
    }

    public function getSitting(): ?Sitting
    {
        return $this->sitting;
    }

//    public function setSitting(?Sitting $sitting): self
//    {
//        $this->sitting = $sitting;
//
//        return $this;
//    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAttendance(): ?string
    {
        return $this->attendance;
    }

    public function setAttendance(?string $attendance): self
    {
        if (!in_array($attendance, [self::PRESENT, self::ABSENT, self::UNDEFINED, self::REMOTE, self::ABSENT_GIVE_POA, self::ABSENT_SEND_DEPUTY])) {
            throw new InvalidArgumentException('attendance not allowed');
        }
        $this->attendance = $attendance;

        return $this;
    }

    public function isConvocation(): bool
    {
        return self::CATEGORY_CONVOCATION === $this->category;
    }

    public function isInvitation(): bool
    {
        return self::CATEGORY_INVITATION === $this->category;
    }

    public function getDeputy(): ?User
    {
        return $this->deputy;
    }

    public function setDeputy(?User $deputy): static
    {
        $this->deputy = $deputy;

        return $this;
    }

    public function getMandator(): ?User
    {
        return $this->mandator;
    }

    public function setMandator(?User $mandator): static
    {
        $this->mandator = $mandator;

        return $this;
    }
}
