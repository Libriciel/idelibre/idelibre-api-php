<?php

namespace IdelibreApiPhp\HttpClient;



use Fig\Http\Message\StatusCodeInterface;
use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use IdelibreApiPhp\Api\Exception\ForbiddenException;
use IdelibreApiPhp\Api\Exception\IdelibreException;
use IdelibreApiPhp\Api\Exception\NotFoundException;
use IdelibreApiPhp\Api\Exception\UnauthorizedException;
use IdelibreApiPhp\Api\Exception\UnexpectedException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class IdelibreExceptionPlugin implements Plugin
{

    /**
     * Handle the request and return the response coming from the next callable.
     *
     * @see http://docs.php-http.org/en/latest/plugins/build-your-own.html
     *
     * @param RequestInterface $request
     * @param callable $next Next middleware in the chain, the request is passed as the first argument
     * @param callable $first First middleware in the chain, used to to restart a request
     *
     * @return Promise Resolves a PSR-7 Response or fails with an Http\Client\Exception (The same as HttpAsyncClient)
     * @throws \Exception
     */
    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        return $next($request)->then(function (ResponseInterface $response) {
            switch ($response->getStatusCode()) {
                case StatusCodeInterface::STATUS_UNAUTHORIZED:
                    throw new UnauthorizedException($response->getBody(), $response->getStatusCode());
                    break;
                case StatusCodeInterface::STATUS_FORBIDDEN:
                    throw new ForbiddenException($response->getBody(), $response->getStatusCode());
                    break;
                case StatusCodeInterface::STATUS_NOT_FOUND:
                    throw new NotFoundException($response->getBody(), $response->getStatusCode());
                    break;
                case StatusCodeInterface::STATUS_BAD_REQUEST:
                case StatusCodeInterface::STATUS_CONFLICT:
                    throw new IdelibreException($response->getBody(), $response->getStatusCode());
                    break;
                case StatusCodeInterface::STATUS_OK:
                case StatusCodeInterface::STATUS_CREATED:
                case StatusCodeInterface::STATUS_NO_CONTENT:
                    // Do nothing
                    break;
                default:
                    throw new UnexpectedException($response->getBody(), $response->getStatusCode());
            }
            return $response;
        });
    }
}
