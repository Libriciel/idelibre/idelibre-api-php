<?php

namespace IdelibreApiPhp\Tests\Api;

use IdelibreApiPhp\Api\v2\ThemeApiWrapper;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Theme;
use IdelibreApiPhp\Tests\Mocker\ClientMock;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ThemeWrapperTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->clientMock = new ClientMock(Client::class);
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $getSetNormalizer = new GetSetMethodNormalizer();
        $dateTimeZoneNormalizer = new DateTimeNormalizer([DateTimeNormalizer::TIMEZONE_KEY => 'UTC']);
        $this->serializer = new Serializer([$objectNormalizer, $getSetNormalizer, new ArrayDenormalizer(),$dateTimeZoneNormalizer], [new JsonEncoder()]);
        $this->userApi = $this->clientMock->isApiUser();
    }

    /**
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function testGetAll()
    {

        $apiResponse = '[
            {"id":"c8f5fcfd-f7e3-469e-aae9-8ea78adfe1fc","name":"Th\u00e8me principal 1","lvl":1,"fullName":"Th\u00e8me principal 1"},{
            "id":"0887ecd9-ab31-4581-b7d1-342e7489c8c8","name":"Sous-th\u00e8me 1.1","lvl":2,"fullName":"Th\u00e8me principal 1, Sous-th\u00e8me 1.1"}
        ]';

        $structureId = $this->userApi->getStructure()->getId();
        $mockClient = $this->clientMock->mockClient('get', sprintf(ApiPath::BASE_API_V2_PATH . '/themes', $structureId) , $apiResponse);

        $expected = [];
        foreach (json_decode($apiResponse, true) as $theme) {
            $expected[] = $this->serializer->denormalize($theme, Theme::class);
        }

        $themeWrapper = new ThemeApiWrapper($mockClient);
        $response = $themeWrapper->getAll();

        $this->assertEquals($expected, $response);
        $this->assertCount(2, $response);
        $this->assertEquals($expected[0]->getName(), $response[0]->getName());
    }

    /**
     * @throws \Http\Client\Exception
     * @throws Exception
     * @throws ClientExceptionInterface
     */
    public function testGetOne()
    {
        $apiResponse = '{"id": "5f60c6aa-d9a8-4cd8-bbe8-0024ac1d0023", "name": "Budget", "fullName": "Finance, Budget"}';

        $themeId = json_decode($apiResponse, true)['id'];
        $structureId = $this->userApi->getStructure()->getId();
        $clientMock = $this->clientMock->mockClient('get',sprintf(ApiPath::BASE_API_V2_PATH . '/themes/' . $themeId, $structureId), $apiResponse);

        $expected = $this->serializer->deserialize($apiResponse, Theme::class, 'json');

        $themeWrapper = new ThemeApiWrapper($clientMock);
        $response = $themeWrapper->getOne($themeId);

        $this->assertEquals($expected, $response);
    }

    /**
     * @throws \Http\Client\Exception
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function testCreate()
    {
        $apiResponse = '{"id": "5f60c6aa-d9a8-4cd8-bbe8-0024ac1d0023", "name": "Budget", "fullName": "Finance, Budget"}';
        $theme = $this->serializer->deserialize($apiResponse, Theme::class, 'json');
        $structureId = $this->userApi->getStructure()->getId();
        $clientMock = $this->clientMock->mockClient('post', sprintf(ApiPath::BASE_API_V2_PATH . '/themes', $structureId), $apiResponse);

        $themeWrapper = new ThemeApiWrapper($clientMock);
        $response = $themeWrapper->create($theme);

        $this->assertEquals($theme, $response);
    }

    /**
     * @throws \Http\Client\Exception
     * @throws Exception
     * @throws ClientExceptionInterface
     */
    public function testEdit()
    {
        $apiResponse = '{"id": "5f60c6aa-d9a8-4cd8-bbe8-0024ac1d0023", "name": "Budget", "fullName": "Finance, Budget"}';
        $theme = $this->serializer->deserialize($apiResponse, Theme::class, 'json');
        $structureId = $this->userApi->getStructure()->getId();
        $clientMock = $this->clientMock->mockClient('put', sprintf(ApiPath::BASE_API_V2_PATH . '/themes/' . $theme->getId(), $structureId), $apiResponse);

        $themeWrapper = new ThemeApiWrapper($clientMock);
        $response = $themeWrapper->edit($theme);

        $this->assertEquals($theme, $response);
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $theme = '{"id": "5f60c6aa-d9a8-4cd8-bbe8-0024ac1d0023", "name": "Budget", "fullName": "Finance, Budget"}';

        $themeId = json_decode($theme, true)['id'];
        $structureId = $this->userApi->getStructure()->getId();

        $clientMock = $this->clientMock->mockClient('delete', sprintf(ApiPath::BASE_API_V2_PATH . '/themes/' . $themeId, $structureId), $theme);

        $themeWrapper = new ThemeApiWrapper($clientMock);
        $response = $themeWrapper->delete($themeId);

        $this->assertEquals(204, $response);

    }
}
