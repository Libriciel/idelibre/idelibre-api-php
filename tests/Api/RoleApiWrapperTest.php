<?php

namespace IdelibreApiPhp\Tests\Api;

use IdelibreApiPhp\Api\v2\RoleApiWrapper;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Enum\ApiPath;
use IdelibreApiPhp\Model\Role;
use IdelibreApiPhp\Tests\Mocker\ClientMock;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RoleApiWrapperTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->clientMock = new  ClientMock(Client::class);
        $this->serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     */
    public function testGetAll()
    {
        $responseApi = '[
            {"id":"17f4b8ba-7a34-4463-9901-88b619a64be3","name":"Admin","prettyName":"Administrateur"},
            {"id":"230a1c1d-eaec-4fb7-9ba7-d7ac47dc97bb","name":"Actor","prettyName":"Elu"},
            {"id":"eb74e130-b142-476c-9439-f06b58472a17","name":"Deputy","prettyName":"Suppl\u00e9ant"}
         ]';

        $structureId = $this->clientMock->isApiUser()->getStructure()->getId();
        $mockClient = $this->clientMock->mockClient('get', sprintf(ApiPath::BASE_API_V2_PATH . '/roles', $structureId), $responseApi);

        $expected = [];
        foreach (json_decode($responseApi, true) as $role) {
            $expected[] = $this->serializer->denormalize($role, Role::class);
        }

        $roleApiWrapper = new RoleApiWrapper($mockClient);
        $response = $roleApiWrapper->getAll();

        $this->assertEquals($expected, $response);
        $this->assertCount(3, $response);
    }
}
