<?php

namespace IdelibreApiPhp\Tests\Fixtures;


use IdelibreApiPhp\Model\Type;

class TypeFixtures
{
    public function genTypes(): array
    {
        $types = [];

        for ($i=0; $i < 5; $i++) {
            $type = (new Type());
            $type->setName('type' . $i);
            $type->setIsSms(true);
            $type->setIsComelus(false);
            $type->setIsSmsGuests(true);
            $type->setIsSmsEmployees(false);
            $types[] = $type;
        }
        return $types;
    }

}
