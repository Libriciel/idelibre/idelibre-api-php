<?php

namespace IdelibreApiPhp\Tests\Fixtures;

use IdelibreApiPhp\Model\Timezone;

class TimezoneFixtures
{
    public function timezoneParis(): Timezone
    {
        return (new Timezone())
            ->setName('Europe/Paris')
            ->setInfo('Paris timezone');

    }

    public function timezoneNewYork(): Timezone
    {
        return (new Timezone())
            ->setName('America/New_York')
            ->setInfo('New York timezone');
    }

}
