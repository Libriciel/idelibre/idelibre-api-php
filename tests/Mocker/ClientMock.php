<?php

namespace IdelibreApiPhp\Tests\Mocker;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use IdelibreApiPhp\Client;
use IdelibreApiPhp\Model\Api\UserApi;
use IdelibreApiPhp\Model\Structure;
use IdelibreApiPhp\Tests\Fixtures\TimezoneFixtures;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class ClientMock extends TestCase
{
    /**
     * @throws Exception
     */
    public function mockClient(string $method, string $url, string $jsonFromApi): Client|MockObject
    {
        $responseBody = $this->createMock(ResponseInterface::class);
        $responseStream = $this->createMock(StreamInterface::class);

        match ($method) {
            'delete' => $responseBody->method('getStatusCode')->willReturn(204),
            'get', 'put' => $responseBody->method('getStatusCode')->willReturn(200),
            'post' => $responseBody->method('getStatusCode')->willReturn(201),
        };

        $responseStream->method('getContents')->willReturn($jsonFromApi);
        $responseBody->method('getBody')->willReturn($responseStream);

        $clientMock = $this->createMock(Client::class);
        $clientMock->setAuthentication($this->isApiUser()->getToken());
        $httpMethod = $this->createMock(HttpMethodsClientInterface::class);
        $httpMethod->expects(self::once())
            ->method($method)
            ->with($url)
            ->willReturn($responseBody);

        $clientMock->expects(self::once())
            ->method('isApiUser')
            ->willReturn($this->isApiUser());

        $clientMock->expects(self::once())
            ->method('getHttpClient')
            ->willReturn($httpMethod);

       return $clientMock;
    }

    /**
     * @throws Exception
     */
    public function mockMultipartClient(string $method, string $url, $apiResponse): Client|MockObject
    {
        $clientMock = $this->createMock(Client::class);
        $clientMock->setAuthentication($this->isApiUser()->getToken());

        $clientMock->expects(self::once())
            ->method('isApiUser')
            ->willReturn($this->isApiUser());


        $clientMock->expects(self::once())
            ->method($method)
            ->with($url, self::callback(fn($multi) => $multi instanceof MultipartStreamBuilder))
            ->willReturn($apiResponse);

        return $clientMock;
    }

    public function isApiUser(): UserApi
    {
        $structure = (new Structure())
            ->setId('d86ee5fd-78a1-4b20-9e2a-921be09954d1')
            ->setName('Libiriciel')
            ->setReplyTo('noreply@contact.com')
            ->setSiren('123456789')
            ->setGroup(null)
            ->setTimezone((new TimezoneFixtures())->timezoneParis())
            ->setSuffix('libriciel')
            ->setIsActive(true)
            ->setCanEditReplyTo(true);

        return (new UserApi())
            ->setId('b88e85b9-f729-44d6-9451-d5e4050faa45')
            ->setName('Pedro Pascal')
            ->setToken('123456789')
            ->setStructure($structure);
    }

}
